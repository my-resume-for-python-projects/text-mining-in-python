import re
import os
import emoji as emoji
from string import ascii_letters
import pandas


df = pandas.read_csv('textFile.csv')
df_list = df.values.tolist()
clean_list = []
for s in df_list:
    text1 = emoji.get_emoji_regexp().sub(u'', str(s))      # remove emoji
    tr_table = str.maketrans({c: None for c in ascii_letters})  # code for remove all English alphabets (if your data text is not in English)
    text2 = str(text1).translate(tr_table)   # remove English alphabets (if you need)
    text3 = re.sub(r'[{}()=-_0123456789,!@#$%^&*+\/""''|~`<>;-÷×]', r'', text2)   # remove extra characters
    text4 = text3.replace("'", '')     # remove Quotes
    text5 = re.sub('\n', '', text4)    # remove Enters
    text6 = re.sub(' +', ' ', text5)   # remove multiple spaces
    text7 = re.sub(r'\.+', '', text6)  # remove multiple dots
    clean_list.append(text7)

df = pandas.DataFrame(clean_list)
df.to_csv('cleanedTextFile.csv', index = False, header = False)

if os.path.isfile('cleanedTextFile.csv'):
    print("Your text is cleaned, now. And is written in the .csv file. Enjoy!")
else:
    print("Oh, an error accrued! I can't find the file!")

