Before starting to analyze text data, you need to clean texts. I do this in python as something like file clean_text.py. 

Note 1: If you think you may need to count sentences during data analyzing, don't remember to not remove punctuations. It will omit all dots for you!

Note 2: If main text data is in English, you must clean lines 13 & 14 in the uploaded file. They would remove all English characters for you!

Note 3: If texts are in English, but you have many unnecessary alphabetic characters, you may have to select the intended part from the whole string, For example:

Intended_text = re.search ('start_marker (.+?) end_marker', text)

In this code, start_marker is a string that you need after that, and end_marker is a string you need up to that. Such as:


Text = "This is my text. Blah blah blah….."
Intended_text = re.search (is (.+?) blah', text)
Output:  my text. Blah

It would be very useful, because when you fetch messages from telegram, using telethon library, the fetched message is something like this:

Message(id=5320, to_id=PeerChannel(channel_id=1000027292), date=datetime.datetime(2020, 7, 12, 6, 52, 11, tzinfo=datetime.timezone.utc), message='your message comes here! ', out=False, mentioned=False, media_unread=False, silent=False, post=True, from_scheduled=False, legacy=False, edit_hide=False, from_id=None, fwd_from=None, via_bot_id=None, reply_to_msg_id=None, media=MessageMediaDocument(document=Document(id=6044166948625842957, access_hash=6832392955942317392, file_reference=b'\x02;\x9b4\x9c\x00\x00\x14\xc8_\x0b/\x8d\xf3\x04\x01\xe1\xc4Z\x08)\x8a~\x80\xbdV\x02q\xbc', date=datetime.datetime(2018, 2, 7, 21, 2, 51, tzinfo=datetime.timezone.utc), mime_type='audio/mpeg', size=7921271, dc_id=4, attributes=[DocumentAttributeAudio(duration=1314, voice=False, title='www.ParvaresheAfkar.com', performer='Pasokhe Soalate 81', waveform=None), DocumentAttributeFilename(file_name='Pasokhe Soalate 81.mp3')], thumbs=[PhotoSize(type='s', location=FileLocationToBeDeprecated(volume_id=421205069, local_id=17468), w=90, h=90, size=2910)], video_thumbs=[]), ttl_seconds=None), reply_markup=None, entities=[MessageEntityHashtag(offset=3, length=15), MessageEntityHashtag(offset=28, length=11), MessageEntityMention(offset=693, length=12)], views=1309, edit_date=None, post_author=None, grouped_id=None, restriction_reason=[])


And I needed just the message text that participants had sent.
