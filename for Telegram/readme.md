1.	Version of codes: 
All of codes are written for python3. For python2, you may need some changes.

2.	Telegram API credentials: 
Connecting to telegram via python, you need your API credentials. So, go to the website: https://my.telegram.org and follow the path. A help documentation is here: https://core.telegram.org/api/obtaining_api_id

3.	Your security: 
Putting your API credentials in your code is not secure. I highly recommend you to create a config file and use it. In config file, you don't need to put string in quotes. An example file is uploaded, named config.ini

4.	If you need proxy: 
If you are limited to connect to telegram in your country (such as me, in Iran), you have to use a valid proxy, too. If you have a good proxy on your computer, that's OK! If not, you need to connect it via python. First, install the package PySocks using the code: 
pip install PySocks 
Then, find a good and valid proxy address and replace the code in the file telegramDataScraping.py (line 26):
TelegramClient(username, api_id, api_hash)

 With this:
TelegramClient(username, api_id, api_hash, proxy=(blah blah blah))

5.	Participants: 
Fetching all participants are allowed just for groups and chats, not channel. Only if you are the admin of a channel, you can get members. It means, just you can get those data which you can see on your telegram. If you can't see something, you are not allowed to fetch it. This is because of the security of https://telegram.org. I have put a separated code for fetching participants in the file, named forReadParticipants.py
 
6.	Reference
Reference of all these codes are: https://docs.telethon.dev/en/latest/ . You can find the latest documentation here, all the time.
