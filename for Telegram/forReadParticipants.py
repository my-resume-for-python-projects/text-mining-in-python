# This is an extra part for getting all participants of a group. You can't use rhis for channels, unless you are an admin. 
# It means you can read messages in both channels and groups, but you can't do this for participants, if you can see them on your telegram app!

offset = 0
limit = 10000
all_participants = []

while True:
    participants = client(GetParticipantsRequest(
        my_channel, ChannelParticipantsSearch(''), offset, limit,
        hash=0
    ))
    if not participants.users:
        break
    all_participants.extend(participants.users)
    offset += len(participants.users)

# create a list to add members data to, and then write a json file:
all_user_details = []
for participant in all_participants:
    all_user_details.append(
        {"id": participant.id, "first_name": participant.first_name, "last_name": participant.last_name,
         "user": participant.username, "is_bot": participant.bot})

with open('participants.json', 'w') as outfile:
    json.dump(all_user_details, outfile)


