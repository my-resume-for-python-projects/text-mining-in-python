import configparser
import os
import telethon.sync
from telethon import TelegramClient
from telethon.errors import SessionPasswordNeededError
from telethon.tl.functions.messages import (GetHistoryRequest)
from telethon.tl.types import (PeerChannel)
import re
import pandas


# Reading Config file:
config = configparser.ConfigParser()
config.read("config.ini")

# Setting configuration values
api_id = config['Telegram']['api_id']
api_hash = config['Telegram']['api_hash']
api_hash = str(api_hash)

phone = config['Telegram']['phone']
username = config['Telegram']['username']


# Create the client and connect to your connect to telegram for getting data:
client = TelegramClient(username, api_id, api_hash)
client.start()
print("Client Created")
# Ensure you're authorized
if not client.is_user_authorized():
    client.send_code_request(phone)
    try:
        client.sign_in(phone, input('Enter the code: '))
    except SessionPasswordNeededError:
        client.sign_in(password=input('Password: '))


# connect to a channel or group on Telegram:
user_input_channel = input("enter the channel/ group URL:")
source_name = input("enter the name of the group / channel in English: ")
if user_input_channel.isdigit():
    entity = PeerChannel(int(user_input_channel))
else:
    entity = user_input_channel

my_channel = client.get_entity(entity)


# Set up needed variable:
offset_id = 0
limit = 500000
all_messages = []
total_messages = 0
total_count_limit = 0   #If you set this to 0, the script will get all messages from the channel.


# Start to get all messages from the channel / group which you have connected to:
while True:
    print("Current Offset ID is:", offset_id, "; Total Messages:", total_messages)
    history = client(GetHistoryRequest(
        peer= my_channel,
        offset_id= offset_id,
        offset_date= None,
        add_offset= 0,
        limit= limit,
        max_id= 0,
        min_id= 0,
        hash= 0
    ))
    if not history.messages:
        break
    messages = history.messages
    for message in messages:
        message = str(message)
        raw_message = re.search("message= (.+?) , out=", message)   # if you don't need all details about messages, can use this part of code, to delete all unnecessary details,
        #if raw_message:
            #found = raw_message.group(0)
        all_messages.append(message)     # Or:  all_messages.append(raw_message)
    offset_id = messages[len(messages) - 1].id
    total_messages = len(all_messages)
    if total_count_limit != 0 and total_messages >= total_count_limit:
        break


# write the result in a .csv file as a list:
messages_filename = source_name + '_Messages.csv'

df = pandas.DataFrame(all_messages)
df.to_csv(messages_filename, index = False, header = False)

# check if the file is written or not:
if os.path.isfile(messages_filename):
    print("All messages you need are written in the .csv file. Enjoy!")
else:
    print("Oh, an error accrued!")
